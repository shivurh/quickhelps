#!/usr/bin/env python
# Function to take 2 lists, return if they ahve atleast 1 common number
def common_data(list1,list2):
	result=False
	for x in list1:
		for y in list2:
			if x==y:
				result=True
	return result

list1=raw_input("Enter items for list1:")
list2=raw_input("Enter items for list2:")
lis1=list(list1.split())
lis2=list(list2.split())
r=common_data(lis1,lis2)
if r: 
	print "At least 1 common item",r
else:
	print "No common items"
