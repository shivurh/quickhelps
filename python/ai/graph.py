#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
def f(n):
	cnt=0
	for i in range(1,n+1):
		k=i
		while k*k<=n:
			cnt+=1
			k+=1
	return cnt
N=10000
x=list(range(N))
y=[f(i) for i in x]

z=np.poly1d(np.polyfit(x,y,1))

plt.title(z)
plt.plot(x,z(x))
plt.show()
