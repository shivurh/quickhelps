#!/usr/bin/env python
import sys
import boto3
s3=boto3.resource("s3")
try:
	bucket_name=sys.argv[1]
except IndexError as error:
	print "Bucket Name was not supplied"

try:
	object_name=sys.argv[2]
except IndexError as error:
	print "Object Name was not supplied"

try:
	response=s3.Object(bucket_name,object_name).put(Body=open(object_name,'rb'))
	print response
except Exception as error:
	print "Usage : \n", sys.argv[0]," BUCKET-NAME OBJECT-NAME"
