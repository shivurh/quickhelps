#!/usr/bin/env python
import sys
import boto3

ec2=boto3.resource('ec2')
if len(sys.argv) <= 1:
	print "Usage : python ", sys.argv[0], " instnace_id1, instance_id2, instnace_id3 ..."
	sys.exit(1)
for instance_id in sys.argv[1:]:
	instance=ec2.Instance(instance_id)
	response=instance.terminate()
	print response
