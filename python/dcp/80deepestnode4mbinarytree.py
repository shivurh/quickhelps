#!/usr/bin/env python

#This problem was asked by Google.
#
#Given the root of a binary tree, return a deepest node. For example, in the following tree, return d.
#
#    a
#   / \
#  b   c
# /
#d
#



def deepest(node):
    
    # leaf and its depth
    if node and not node.left and not node.right:
        return (node,1) 
    
    # then the deepest node is on the right subtree
    if not node.left:
        return increament_depth(deepest(node.right))
    # then the deepest node is on the left subtree
    elif not node.right:
        return increament_depth(deepest(node.left))
    
    # Pick higher depth tuple and the increament its depth
    return increament_depth(max(deepest(node.left), deepest(node.right),key=lambda x: x[1]))

def increament_depth(node_depth_tuple):
    node,depth = node_depth_tuple
    return (ode, depth + 1)
