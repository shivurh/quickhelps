#!/usr/bin/env python
class parent:
	"Base class"
	def __init__(self,name):
		self.name=name
	def displayname(self):
		print "Name",self.name
	def __del__(self):
		class_name=self.__class__.__name__
		print class_name,"destroyed"
class child(parent):
	def __init__(self,name,address):
		self.name=name
		self.address=address
	def displayname(self):
		print "Name:",self.name
		print "Address:",self.address
	def __del__(self):
		class_name=self.__class__.__name__
		print class_name," destroyed"
n=raw_input("Enter a name:")
a=raw_input("Enter address:")
obj=child(n,a)
obj.displayname()
del obj
obj=parent(n)
obj.displayname()
del obj
