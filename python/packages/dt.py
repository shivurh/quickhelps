#!/usr/bin/env python
import time
import datetime
print "Current",datetime.datetime.now()
print "Year",datetime.date.today().strftime("%Y")
print "Month",datetime.date.today().strftime("%B")
print "Week of the year:",datetime.date.today().strftime("%W")
print "Week day of the week:",datetime.date.today().strftime("%w")
print "Day of the year:",datetime.date.today().strftime("%j")
print "Day of the month:",datetime.date.today().strftime("%d")
print "Day of the week:",datetime.date.today().strftime("%A")
